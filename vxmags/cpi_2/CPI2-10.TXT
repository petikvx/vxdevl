[2.10]

                           Virili In The News
                           ------------------
        This section deals with a large amount of stuff, basically, a bunch
    of viruses and stuff that have been in the newspapers and magazines cuz
    all of the damage they have done. Enjoy....


                      There's A Virus In My Software

                      Mischief-makers at the computer
                     are deliberately endangering data

                             By Philip J. Hilts

                        Washington Post Staff Writer

      The Washington Post Weekly Edition, Page #38.  May 23-29, 1988.

     Tiny programs that are deliberately cause mischief are epidemic among
computers and causing nervousness among those who monitor them.  Since the
first tests of the notion in 1983 that machines can catch and spread
"information diseases," the computer world has reached the point at which as
many as thirty instances of "computer virus" have been reported in the past
year, affecting tens of thousands of U.S. computers alone.

     Such viruses have been found at the National Aeronautics and Space
Administration, International Business Machines Corporation, the House of
Representatives, at least six universities, several major computer networks
such as Comp-u-serve and several businesses, including the world's largest
computer-service company, the $4.4 billion Electronic Data Systems
Corporation of Dallas, Texas.

     Written by malicious programmers, the viruses are sneaked into computer
systems by piggybacking them on legitimate programs and messages.  There,
they may be passed along or instructed to wait until a prearranged moment to
burst forth and destroy data.

     Hundreds of computers at the Hebrew University of Jerusalem and other
places in Israel were hit last fall by a virus designed to spread and then,
in one swipe on a Friday the thirteenth, destroy all data in any computer it
could reach.

     If not for an error by it's author, who has not been caught, the virus
could have caused devastation among micro-computers in Israel and other
nations.  The virus did not check to see whether it already had infected a
program and so infected some computers hundreds of times, crowding their
memories enough to call attention to itself.

     In a seven-month campaign, programmers in Israel hastened to find
infected machines and ensure that the smallest number would be affected
before Friday, May 13th.  Officials say they initially thought that the
infection was connected with the anniversary of the last day that Palestine
existed as a political entity but subsequently decided that it most likely
involved just Friday the thirteenth.

     Apparently, the campaign was successful; there has been no word of
substantial damage.  This past Friday the thirteenth is this year's only such
day.

     At the Aldus Corporation of Seattle, Washington, a major software maker,
executives are huddling with lawyers to try to determine whether
international spread of such diseases is illegal.  No virus cases have been
taken to court.

     At N.A.S.A. headquarters in Washington, several hundred computers had to
be resuscitated after being infected.  N.A.S.A. officials have taken
precautions and reminded their machines' users to follow routine computer
hygiene: Don't trust foreign data or strange machines.

     Viruses have the eerie ability to perch disguised among legitimate data
just as biological viruses hide among genes in human cells, then spring out
unexpectedly, multiplying and causing damage.  Experts say that even when
they try to study viruses in controlled conditions, the programs can get out
of control and erase everything in a computer.  The viruses can be virtually
impossible to stop if their creators are determined enough.

     "The only way to protect every-body from them is to do something much
worse than the viruses: Stop talking to one another with computers," says
William H. Murray, an information-security specialist at Ernst and Whinney
financial consultants in Hartford, Connecticut.

     Hundreds of programs and files have been destroyed by viruses, and
thousands of hours of repair or prevention time have been logged.
Programmers have quickly produced antidote programs with such titles as
"Vaccine," "Flu Shot," "Data Physician," "Syringe."

     Experts says known damage is minimal compared with the huge, destructive
potential.  They express the hope that the attacks will persuade computer
users to minimize access to programming and data.

     "What we are dealing with here is the fabric of trust in society," says
Murray.  "With computer viruses, we have a big vulnerability."

     Early this year, Aldus Corporation discovered  that a virus had been
introduced that infected at least five-thousand copies of a new drawing
program called Freehand for the Macintosh computer.  The infected copies were
packaged, sent to stores and sold.  On March 2, the virus interrupted users
by flashing this message on their screens:

     "Richard Brandow, publisher of MacMag, and its entire staff would like
to take this opportunity to convey their universal message of peace to all
Macintosh users around the world."

     Viruses are the newest of evolving methods of computer mayhem, says
Donn B. Parker, a consultant at SRI International, a computer research firm
in Menlo Park, California.  One is the "Trojan horse," a program that looks
and acts like a normal program but contains hidden commands that eventually
take effect, ordering mischief.  Others include the "time bomb," which
explodes at a set time, and the "logic bomb," which goes off when the
computer arrives at a certain result during normal computation.  The "salami
attack" executes barely noticeable results small acts, such as shaving a
penny from thousands of accounts.

     The computer virus has the capability to command the computer to make
copies of the virus and spread them.  A virus typically is written only as a
few hundred characters in a program containing tens of thousands of
characters.  When the computer reads legitimate instructions, it encounters
the virus, which instructs the computer to suspend normal operations for a
fraction of a second.

     During that time, the virus instructs the computer to check for other
copies of itself and, if none is found, to make and hide copies.  Instruction
to commit damage may be included.  A few infamous viruses found in the past
year include:

[]   The "scores" virus.  Named after a file it spawns, it recently entered
     several hundred Macintosh computers at N.A.S.A. headquarters.  "It looks
     as if it searching for a particular Macintosh program with a name that
     no one recognizes," spokesman Charles Redmond says.

         This virus, still spreading, has reached computers in Congress'
     information system at the National Oceanic and Atmospheric
     Administration and at Apple Computer Incorporated's government-systems
     office in Reston, Virginia.  It has hit individuals, businesses and
     computer "bulletin boards" where computer hobbyists share information.
     It apparently originated in Dallas, Texas and has caused damage, but
     seemingly only because of its clumsiness, not an instruction to do
     damage.

[]   The "brain" virus.  Named by its authors, it was written by two brothers
     in a computer store in Lahore, Pakistan, who put their names, addresses
     and phone number in the virus.  Like "scores," it has caused damage
     inadvertently, ordering the computer to copy itself into space that
     already contain information.

[]   The "Christmas" virus.  It struck last December after a West German
     student sent friends a Christmas message through a local computer
     network.  The virus told the receiver's computer to display the
     greeting, then secretly send the virus and message to everyone on the
     recipient's regular electronic mailing list.

         The student apparently had no idea that someone on the list had
     special, restricted access to a major world-wide network of several
     thousand computers run by I.B.M.  The network broke down within hours
     when the message began multiplying, stuffing the computers' memories.
     No permanent damage was done, and I.B.M. says it has made repetition
     impossible.

     Demonstrations have shown that viruses can invade the screens of users
with the highest security classification, according to Fred Cohen of
Cincinnati, a researcher who coined the term "computer Viruses."  A standard
computer-protection device at intelligence agencies, he says, denies giving
access by a person at one security level to files of anyone else at a higher
level and allows reading but denies writing of files of anyone lower.

     This, however, "allows the least trusted user to write a program that
can be used by everyone" and is "very dangerous," he says.

     Computers "are all at risk," says Cohen, "and will continue to be... not
just from computer viruses.  But the viruses represent a new level of threat
because of their subtleness and persistence."


1.) Computer "viruses" are actually immature computer programs.  Most are
    written by malicious programmers intent on destroying information in
    computers for fun.

2.) Those who write virus programs often conceal them on floppy disks that
    are inserted in the computer.  The disks contain all programs needed to
    run the machine, such as word processing programs, drawing programs or
    spread sheet programs.

3.) A malicious programmer makes the disk available to others, saying it
    contains a useful program or game.  These programs can be lent to others
    or put onto computerized: "bulletin boards" where anyone can copy them
    for personal use.

4.) A computer receiving the programs will "read" the disk and the tiny virus
    program at the same time.  The virus may then order the computer to do a
    number of things:

    A.) Tell it to read the virus and follow instructions.

    B.) Tell it to make a copy of the virus and place it on any disk inserted
        in the machine today.

    C.) Tell it to check the computer's clock, and on a certain date destroy
        information that tells it where data is stored on any disk: if an
        operator has no way of retrieving information, it is destroyed.

    D.) Tell it not to list the virus programs when the computer is asked for
        an index of programs.

5.) In this way, the computer will copy the virus onto many disks--perhaps
    all or nearly all the disks used in the infected machine.  The virus may
    also be passed over the telephone, when one computer sends or receives
    data from another.

6.) Ultimately hundreds or thousands of people may have infected disks and
    potential time bombs in their systems.


              -----------------------------------------------
                    'Virus' infected hospital computers,
                    led to epidemic of software mix-ups
              -----------------------------------------------
                         From the San Diego Tribune
                               March 23, 1989


     BOSTON (UPI) -- A "virus" infected computers at three Michigan hospitals
last fall and disrupted patient diagnoses at two of the centers in what appears
to be the first such invasion of a medical computer, it was reported yesterday.

     The infiltration did not harm any patients but delayed diagnoses by
shutting down computers, creating files of non-existent patients and garbling
names on patient records, which could have caused more serious problems, a
doctor said.

     "It definitely did affect care in delaying things and it could have
affected care in terms of losing this information completely," said Dr. Jack
Juni, a staff physician at the William Beaumont Hospitals in Troy and Royal Oak,
Mich., two of the hospitals involved.

     If patient information had been lost, the virus could have forced doctors
to repeat tests that involve exposing patients to radiation, Juni said
yesterday.  The phony and garble files could have caused a mix-up in patient
diagnosis, he said.

     "This was information we were using to base diagnoses on," said Juni, who
reported the case in a letter in The New England Journal of Medicine. "We were
lucky and caught it in time."

     A computer virus is a set of instructions designed to reproduce and spread
from computer to computer.  Some viruses do damage in the process, such as
destroying files or overloading computers.

     Paul Pomes, a computer virus expert at the University of Illinois in
Champaign, said this was the first case he had heard of in which a virus had
disrupted a computer used for patient care or diagnosis in a hospital.

     Such disruptions could become more common as personal computers are used
more widely in hospitals, Juni and Pomes said.  More people know how to program
-- and therefore sabotage -- personal computers than the more specialized
computers that previously have been used, Pomes said.

     The problem in Michigan surfaced when a computer used to display images
used to diagnose cancer and other diseases began to malfunction at the 250-bed
Troy hospital in August 1988.

     In October, Juni discovered a virus in the computer in the Troy hospital.
The next day, Juni found the same virus in a similar computer in the 1,200-bed
Royal Oak facility, he said.

     The virus apparently arrived in a program in a storage disk that was part
of the Troy computer system, he said.  It probably was spread inadvertently to
the Royal Oak computer on a floppy disk used by a resident who worked at both
hospitals to write a research paper, he said.

      The virus also spread to the desk-top computers at the University of
Michigan Medical Center in Ann Arbor, where it was discovered before it caused
problems.


           "Prosecutor Wins Conviction In Computer Data Destruction"

                               September 21, 1988


     Fort Worth, Texas (AP) - A former programmer has been convicted of planting
a computer "virus" in his employer's system that wiped out 168,000 records and
was activated like a timb bomb, doing its damage two days after he was fired.

     Tarrant County Assistant District Attorney Davis McCown said he believes e
is the first prosecutor in the country to have someone convicted for destroying
computer records using a "virus."

     "We've had people stealing through computers, but not this type of case,"
McCown said. "The basis for this offense is deletion."

     "It's very rare that the people who spread the viruses are caught," said
John McAfee, chairman of the Computer Virus Industry Association in Santa Clara,
which helps educate the public about viruses and find ways to fight them.

     "This is absolutely the first time" for a conviction, McAfee said.

     "In the past, prosecutors have stayed away from this kind of case because
they're too hard to prove," McCown said yesterday. They have also been reluctant
because the victim doesn't want to let anyone know there has been a breach of
security."

     Donald Gene Burleson, 40, was convicted of charges of harmful access to a
computer, a third-degree feloy that carries up to 10 years in prison and up to
$5,000 in fines.

     A key to the case was the fact that State District Judge John Bradshaw
allowed the computer program that deleted the files to be introduced as
evidence, McCown said. It would have been difficult to get a conviction
otherwise, he said.

     The District Court jury deliberated six hours before bringing back the
first conviction under the state's 3-year-old computer sabotage law.

     Burleson planted the virus in revenge for his firing from an insurance
company, McCown said.

     Jurors were told during a technical and sometimes-complicated three-week
trial that Burleson planted a rogue program in the computer system used to store
records at USPA and IRA Co., a Fort Worth-based insurance and brokerage firm.

     A virus is a computer program, often hidden in apparently normal computer
software, that instructs the computer to change or destroy information at a
given time or after a certain sequence of commands.

     The virus, McCown said, was activated Sept. 21, 1985, two days after
Burleson was fired as a computer programmer, because of alleged personality
conflicts with other employees.

     "There were a series of programs built into the system as early as Labor
Day (1985)," McCown said. "Once he got fired, those programs went off."

     The virus was discovered two days later, after it had eliminated 168,00
payroll records, holding up company paychecks for more than a month. The virus
could have caused hundreds of thousands of dollars in damage to the system had
it continued, McCown said.
