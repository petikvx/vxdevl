    Continuing with recent report off the Internet on the Mutating Engine:
    Part 4/4
                 *************************************
                                 22 Jun 92

                            Mutation Engine Report
            Copyright (c) 1992 by  VDS Advanced Research Group
                           All Rights Reserved

                    V. A Simple Message

      It is dangerous to assume that scanning is adequate since
there are some products that can detect MtE-based viruses 100% of
the time. We identified at least two ways to make MtE less
predictable. Of course, such information will not be disseminated.
However, considering the availability of MtE to the hackers all
around the world, and the "glory" Dark Avenger will enjoy due to
media hype, it's only a matter of time such improvements will be
discovered by irresponsible individuals. Besides, this may start a
new trend among virus writers to create things like MtE. Keeping up
with new virus signatures was hard enough (though manageable), but
keeping up with many mutation engines is not going to be trivial.
Unfortunately, locking up these "mutant engineers" is not a
practical solution, and not even legally possible in many parts of
the world.
      The message is clear. The first line of defense against
viruses is NOT using scanners. Although they proved to be very
useful, you are highly encouraged to consider other approaches such
as integrity checkers as a first line of defense. Even the less
sophisticated integrity checkers have a better chance to catch
mutating viruses, long before their developers get a chance to
analyze the virus samples. The reason is that viruses have a
tendency to modify existing code to propagate in most cases. Their
spread can be controlled using a non-virus-specific solution that
concentrates on the main characteristic of most viruses. Such an
approach is not only more cost-effective but also more secure. If
your company still relies on a virus scanner to protect its PC-
based computing resources against viruses, you are walking on thin
ice.

- -------------------------------------------------------------
Regards,

Tarkan Yetiser
VDS Advanced Research Group               P.O. Box 9393
(410) 247-7117                            Baltimore, MD 21228
e-mail:  tyetiser@ssw02.ab.umd.edu


END OF REPORT
                 *************************************

  I'll keep my eye open for more if you found that interesting.

        -- Gene Splicer

P.S.  By the way, has anybody managed to disassemble a recompilable version
      of the Whale?  I would like to take a closer look at its internals.

